<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Joomalungma All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Joomla 2.5 compatibility
defined('DS') or define('DS', DIRECTORY_SEPARATOR );

/**
 * Joomla! Jemmy Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	System.jemmy
 */
class plgSystemJemmy extends JPlugin
{

    private $tokenKey;
    private $in;
    private $cookie;
    private $error;
    private $mess = array();

	/**
	 * Constructor
	 *
	 * @access	protected
	 * @param	object	$subject The object to observe
	 * @param	array	$config  An array that holds the plugin configuration
	 * @since	1.0
	 */
	function __construct(& $subject, $config)
	{
        parent::__construct($subject, $config);
        $this->in = JFactory::getApplication()->input;
        $this->cookie = new JInputCookie();
	}

	/**
	* Converting the site URL to fit to the HTTP request
	*
	*/
	function onAfterInitialise()
	{

        $app	= JFactory::getApplication();
        $user	= JFactory::getUser();

        if ($app->isAdmin() or !$user->get('guest')) {
            return;
        }

        $this->loadLang();
        // Check Params
        try {
            if( !$this->params->get('password','') )throw new Exception(JText::_('JEMMY_PLUGIN_REQUIRED_PASS'));
            if ( !$this->params->get('salt','')) throw new Exception(JText::_('JEMMY_PLUGIN_REQUIRED_SALT'));



            $this->tokenKey = $this->mess('jemmyToken');
            $this->mess = array(
                'password'  => $this->mess($this->params->get('password','')),
                'success'   => $this->mess('success'),
            );

            if( $this->checkCookie() ) return;
            else {
                if( !(bool)$this->in->get($this->tokenKey , 0 )){
                    $this->showForm();
                }
                else {
                    if ( $this->checkForm() ) return;
                    else {
                        throw new Exception(JText::_('JEMMY_WRONG_PASS'));
                    }
                }
            }
        } catch(Exception $e) {
            $this->error = $e->getMessage();
            $this->showForm();
            exit();
        }
	}
    private function checkForm(){
        $pass = $this->in->getString('password','');
        if( strtolower($pass) == $this->params->get('password','') ) {

            $expire = $this->params->get('remember') ? time() + 60 * 60 * 24 * 30 : 0;
            $this->cookie->set( $this->mess['success'], $this->mess['password'] ,$expire );
            return true;
        }
        else return false;
    }
    private function showForm(){
        require_once dirname(__FILE__) . DS . 'templates' . DS . 'form.php';
        exit();
    }
    private function mess($str){
        return md5($this->params->get('salt') . $str);
    }
    private function checkCookie(){
        if( $this->cookie->getString($this->mess['success'],'' ) == $this->mess['password'] ) return true;
        else return false;
    }

    private function loadLang(){
        $language =JFactory::getLanguage();
        $extension = 'plg_system_jemmy';
        $base_dir =  JPATH_ADMINISTRATOR;
        $language_tag = $language->getTag(); // loads the current language-tag
        $language->load($extension, $base_dir, $language_tag, true);
    }
}
