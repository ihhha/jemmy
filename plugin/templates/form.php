<?php
defined('_JEXEC') or die('Restricted access'); ?>

<!DOCTYPE html>
<html lang="en-gb" dir="ltr" >
<head>
    <link href="<?php echo JURI::base();?>plugins/system/jemmy/templates/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo JURI::base();?>plugins/system/jemmy/templates/bootstrap/jemmy.css" rel="stylesheet">
</head>
<body>
<div class="content-wrap">
    <div class="welcome">

        <form action="" method="post"  class="form-inline">
            <input name="<?php echo $this->tokenKey;?>" value="1" type="hidden">
            <input name="password" value="" type="password" placeholder="<?php echo JText::_('JEMMY_PLUGIN_PASSWORD');?>"/>
            <input type="submit" value="<?php echo JText::_('JEMMY_PLUGIN_ENTER');?>" class="btn btn-danger"/>
        </form>
        <?php if($this->error): ;?>
        <div class="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $this->error;?>
        </div>
        <?php endif;?>

    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?php echo JURI::base();?>plugins/system/jemmy/templates/bootstrap/bootstrap.min.js"></script>
</body>
</html>